my amazing project changelog

## v1.3.0

- release rules
  - rule 46
  - rule 47
  - rule 48
  - rule 49
  - rule 50

## v1.2.1

- fix rule 45

## v1.2.0

- release rules
  - rule 45

## v1.1.1
- fix rule 41

## v1.1.0

- release rules
  - rule 41
  - rule 42
  - rule 43
  - rule 44

## v1.0.9

- release rules
  - rule 37
  - rule 38
  - rule 39
  - rule 40

## v1.0.8-bugfix

- fix rule 35

## v1.0.8

- release rules
  - rule 35
  - rule 36

## v1.0.7

- release more rules
  - rule 29
  - rule 30
  - rule 31
  - rule 32
  - rule 33
  - rule 34

## v1.0.6-bugfix

- fix rule 26

## v1.0.6

- release more rules
  - rule 25
  - rule 26
  - rule 27
  - rule 28

## v1.0.5

- release more rules
  - rule 23
  - rule 24

## v1.0.4

- release some rules
  - rule 19
  - rule 20

  - rule 21
  - rule 22

## v1.0.3-bugfix

- fix rule 17

## v1.0.3

- add rule 17
- add rule 18

## v1.0.2-bugfix

- third release bugfix

## v1.0.2

- third release

## v1.0.1

- second release

## v1.0.0

- initial release
